import os
import csv
from data_loader import SpeechCorpus
from utils.config import ConfigReader, TrainNetConfig, DataConfig
from model import *
import matplotlib.pyplot as plt

##HYPERPARAMETER
BATCH_SIZE = 1
NUM_CLASSES = 6
LEARNING_RATE = 0.01
EPOCHS = 1000
FEATURE = 128
DIMENSION = 1
BETA = 0.01
TRAIN_LENGTH = 0.8
VALID_LENGTH = 0.15
TEST_LENGTH = 0.2
LENGTH = 5229
CHECKPOINT_DIR = ''
test_dir = 'data/test/'

def test_net():
    graph = tf.Graph()  # Creates a graph
    sess = tf.compat.v1.InteractiveSession(graph=graph)

    config_reader = ConfigReader('config.yml')
    data_config = DataConfig(config_reader.get_test_config())

    print('===INFO====: Load Data')
    with tf.name_scope('input'):
        data_loader = SpeechCorpus(data_config, is_train=False, is_shuffle=True)
        data = data_loader.load_data()
        data = np.expand_dims(data, axis=2)

    print('===INFO====: Variables initialized')
    layers = get_layer_hyperparameters(NUM_CLASSES)
    define_net_parameter(layers)

    tf_inputs = tf.compat.v1.placeholder(shape=[BATCH_SIZE, FEATURE, DIMENSION], dtype=tf.float32, name='inputs')

    tf_predictions = logits(tf_inputs, layers, BATCH_SIZE)

    saver = tf.train.Saver(tf.global_variables())

    try:
        latest_checkpoint = tf.compat.v1.train.latest_checkpoint(CHECKPOINT_DIR)
        if latest_checkpoint:
            print("Loading model checkpoint {} ...\n".format(latest_checkpoint))
            saver.restore(sess, latest_checkpoint)
            print("Model loaded")
    except FileNotFoundError:
        raise 'Check your pretrained {:s}'.format(CHECKPOINT_DIR)

    mfcc_file = os.listdir(test_dir)
    lines = []
    for i, item in enumerate(data):
        test_predictions = sess.run(tf_predictions, feed_dict={
            tf_inputs: item,
        })
        emotion = np.argmax(test_predictions)
        lines.append(mfcc_file[i]+ ',' + emotion)

    with open('jact_sample.wav', 'w') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(lines)

if __name__ == '__main__':
    test_net()
